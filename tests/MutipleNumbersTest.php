<?php

namespace test;

use PHPUnit\Framework\TestCase;
use app\Challegen\MutipleNumbers;

class MutipleNumbersTest extends TestCase{

    protected $resultOfTheNumbersFrom1To100InString = "1\n2\nFalabella\n4\nIT\nFalabella\n7\n8\nFalabella\nIT\n11\nFalabella\n13\n14\nIntegraciones\n16\n17\nFalabella\n19\nIT\nFalabella\n22\n23\nFalabella\nIT\n26\nFalabella\n28\n29\nIntegraciones\n31\n32\nFalabella\n34\nIT\nFalabella\n37\n38\nFalabella\nIT\n41\nFalabella\n43\n44\nIntegraciones\n46\n47\nFalabella\n49\nIT\nFalabella\n52\n53\nFalabella\nIT\n56\nFalabella\n58\n59\nIntegraciones\n61\n62\nFalabella\n64\nIT\nFalabella\n67\n68\nFalabella\nIT\n71\nFalabella\n73\n74\nIntegraciones\n76\n77\nFalabella\n79\nIT\nFalabella\n82\n83\nFalabella\nIT\n86\nFalabella\n88\n89\nIntegraciones\n91\n92\nFalabella\n94\nIT\nFalabella\n97\n98\nFalabella\nIT\n";

    /** 
     * @test
     */
    public function return_correctly_the_list_of_numbers()
    {
        $numbers = new MutipleNumbers(1, 100);
        $this->assertEquals($this->resultOfTheNumbersFrom1To100InString, $numbers->toString());
    }

    /**
     * @test
     */
    public function return_correctly_the_list_of_numbers_in_string()
    {
        $numbers = new MutipleNumbers(1, 100);
        ob_start();
        $numbers->resultPrint();
        $content = ob_get_contents();
        ob_end_clean();
        $this->assertEquals($this->resultOfTheNumbersFrom1To100InString, $content);
    }

    /**
     * @test
     */
    public function return_correctly_evaluate_the_index_of_a_number()
    {
        $numbers = new MutipleNumbers(1, 10);
        $this->assertEquals(1, $numbers->getMutiple(1));
        $this->assertEquals('Falabella', $numbers->getMutiple(3));
        $this->assertEquals('Integraciones', $numbers->getMutiple(60));
        $this->assertEquals('IT', $numbers->getMutiple(100));
    }
}