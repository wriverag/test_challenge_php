<?php

namespace app\Challegen;

class MutipleNumbers {
    protected $start = 1;
    protected $end = 100;
    protected $list = [];
    protected $intervals = [3, 5];
    protected $multiple = [
        0 => 1,
        3 => 'Falabella',
        5 => 'IT',
        8 => 'Integraciones'
    ];

    public function __construct($start = 1, $end = 100, $intervals = [3, 5]){
        $this->start = $start;
        $this->end = $end;
        $this->intervals = $intervals;
        $this->initNumbers();
    }

    public function initNumbers(){

        for ($i = $this->start; $i <= $this->end; $i++) {
            $number = $this->getMutiple($i);
            $this->list[] = $number;
        }
    }

    public function getMutiple($numbers){
        $index = 0;
        $this->multiple[0] = $numbers;
        foreach ($this->intervals as $multiply) {
            if (($numbers % $multiply) == 0) {
                $index += $multiply;
            }
        }

        return $this->multiple[$index];

    }

    public function toString(){
        return implode("\n", $this->list) . "\n";
    }

    public function resultPrint(){
        echo $this->toString();
    }
}